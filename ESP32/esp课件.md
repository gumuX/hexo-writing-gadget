# 一、基础知识补充

格式化输出

Serial.printf()

%d:指参数以十进制整型输入or输出
%f:浮点数输入or输出(还有%1.2f 等，表示限定小数点前后的位数)
%u:无符号整数
%x:十六进制
%o:八进制

％c用来输出一个字符

％s用来输出一个字符串



int array [[0]][2][2] = {1,2,3,4,5,6};



![image-20210727121115809](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210727121115809.png)

![image-20210727121129799](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210727121129799.png)

内置函数补充

```c++
millis() //功能：就是返回开发板运行当前程序开始的毫秒数
delay()  //延时函数
```



## 1、开发板介绍

![image-20210713105218660](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210713105218660.png)

**l34/28个数据引脚**

**l可用引脚23个**

**lEN按键：重启模组**

**lBoot+EN：进入编程模式**

## 1.2.基本信息介绍

1. 32 bit双核处理器，运算能力高达600 MIPS；
2. 448 KB ROM；
3. 520 KB SRAM；
4. 34个输入/输出接口；
5. **12 bit模数转换器(ADC)；18通道12位**
6. 10个触摸传感器；
7. UART 、I2C 和 SPI 等接口 ；
8. 霍尔传感器；
9. 电机 PWM、LEDPWM；
10. **数模转换器DAC；2通道8位**
11. WiFi +蓝牙+低功耗蓝牙(BLE)
12. 模组的核心是 ESP32-D0WDQ6 芯片

## 1.3模块介绍



|          |      | **ESP32 DEV模块**            |
| -------- | ---- | ---------------------------- |
| 电源     |      | 3.3V                         |
| CPU      |      | Xtensa双核32位LX6            |
| 蓝牙     |      | 符合蓝牙v4.2 BR/EDR和BLE规范 |
| GPIO     |      | 32                           |
| 闪存大小 |      | 最大16MB                     |
| ADC      |      | 12位                         |
| DAC      |      | 2 * 8bit                     |
| UART     |      | 2                            |





![image-20210710134945433](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210710134945433.png)



![image-20210710134958172](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210710134958172.png)



## 2、IO口学习

上拉下拉

![无标题](https://gitee.com/gumuX/figurebed/raw/master/images/无标题.png)

### 2.1数字信号学习

```c++
//引脚
pinMode(pin,mode)：//设置引脚的工作模式 mode：INPUT/OUTPUT/INPUT_PULLUP/INPUT_DOWN
digitalRead(pin)：//从指定引脚读取外部输入的数字信号
digitalWrite(pin,value)：//向指定引脚输出高低电平数字信号

```

### 2.2模拟信号学习

```c++
//模拟信号输入函数ADC
analogRead()：//从指定引脚读取模拟信号，获取返回值。
analogSetWidth()：//设置analogRead()函数的取样分辨率。
//keyVal = map(keyVal,0,4095,0,255);
```



```c++
//模拟信号输出函数 -- 基于LEDC   DAC
ledcSetup(channel,freq,bit_num) //功能：设定指定LEDC通道参数设置。freq频率。bit_num：占空比的分辨率
ledcAttachPin(pin,channel) //功能：将指定通道channel上产生的PWM信号输出到GPIO引脚
ledcWrite(channel,duty) //功能：向指定通道（channel）输出指定占空比（duty）的PWM信号

ledcDetachPin(channel,duty) //功能：取消指定引脚pin的PWM信号输出
ledcWriteTone(pin) //功能：向指定通道（channel）输出指定频率（freq）的音符信号。类似tone()函数
ledcWriteNote(channel, note, octava) //功能：向指定通道（channel）输出指定的音符和音阶的声音。对//ledcWriteTone()进一步封装

```



### **中断程序学习**

```C++
attachInterrupt( pin, function, mode)//功能：初始化中断 （引脚,中断回调函数,5种中断触发模式）
detachInterrupt( pin ); //功能：关闭指定引脚的中断功能
//RISING，FALLING，CHANGE，ONLOW，ONHIGH
```



### 2.3思考题

```
1、 了解ESP32的基本性能及其外设。
2、 ESP32的工作电压是多少？ 3.3
3、 ESP32开发板 EN 按键和 Boot 按键的作用是什么？ 

4、 ESP32开发板数字引脚有哪些工作模式？
5、 ESP32模拟输出分别有哪几种方式？采用LEDC 模拟输出的步骤是什么？
6、 如何在多个引脚实现同步模拟输出？
7、 ESP32模拟输入ADC模块的精度是多少位？12位 模拟数据返回值和电压是线性关系吗？常用的
模拟输入引脚有哪些？
8、 函数 analogSetWidth 的取样精度范围是多少？ 9-12

9、 ESP32DAC 模拟输出值的范围是多少？8位
10、ESP32电容触摸传感器的中断回调函数是什么？
11、ESP32霍尔传感器的返回值和外磁场强度之间的关系是什么？
12、ESP32有几种中断触发模式？中断回调函数是什么？
13、printf函数输出八进制、十进制、十六进制整数、浮点数、字符、字符串对应的格式字符是 什么？
```







## 3、数码管



### 3.1原理学习

数码管有两种：数码管侧边有文字标识，最后两组字母AS代表共阴极数码管，BS代表共阳极数码管



![image-20210714224903877](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210714224903877.png)



![image-20210714224923816](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210714224923816.png)

​              共阳数码管                                                                                                               共阴数码管



![image-20210714225058169](https://zhanghaooss.oss-cn-beijing.aliyuncs.com/blogImg/image-20210714225058169.png)





| 引脚 | 标识  | 说  明                                          |
| ---- | ----- | ----------------------------------------------- |
| 1~7  | Q1~Q7 | 并行数据输出管脚                                |
| 8    | GND   | 电源地                                          |
| 9    | Q7’   | 串行数据输出，级联时接到下一个595的DS端         |
| 10   | MR    | 复位，低电平复位                                |
| 11   | CH_CP | 数据输入时钟线（SCK移位寄存器时钟，上升沿移位） |
| 12   | ST_CP | 输出锁存时钟线（RCK所存寄存器时钟，高电平存储） |
| 13   | OE    | 输出使能，通常置为低，即始终输出                |
| 14   | DS    | 串行数据输入  （DIO、SER）                      |
| 15   | Q0    | 并行数据输出管脚                                |
| 16   | VCC   | 电源                                            |

![image-20210715092301341](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210715092301341.png)

![image-20210715094022693](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210715094022693.png)

级联原理

![image-20210715100605138](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210715100605138.png)



### 3.2函数使用

```c++
bitRead(x, n) //功能： 读取一个数的指定位 (x：想要被读取的数 n：被读取的位，0 是最低有效位（最右边）) 
bitWrite(x, n, b) //功能： 读取一个数的指定位(x：想要被写入的数 ,n：写入的位,b：写入位的数值)
bitSet(x, n) //功能： 为一个数字变量设置一个指定位(x：想要被设置的数 ,n：想要设置的位)
bitClear(x, n)  //功能： 清除一个数值型数值的指定位(将此位设置成 0)(x：想要被设置的数 ,n：想要设置的位)
shiftOut(dataPin, clockPin, bitOrder, value)  
    //功能： 将一个字节的数据一位一位的移出。从最高有效位（最左边）或最低有效位（最右边）开始
    //[dataPin：数据要输出位的引脚,clockPin：时钟脚，当 dataPin 有值时此引脚电平变化,bitOrder：输出位的顺序,value:  要移位输出的数据(byte) ]

```



### 3.3思考题

```
按引脚结构划分，数码管分为哪两类？ 共阴极  共阳极
常见的一位数码管是静态显示还是动态显示？ 动态
数组的下标从0还是1开始？ 0
定义二维数组时，第二维长度声明可否省略？ 不可
sizeof是系统提供的函数还是运算符？其作用是什么？ 所占内存大小
74HC595移位寄存器芯片的工作原理是什么？ Ds引脚、SH_CP引脚、ST_ CP引脚的作用是什么？
74HC595移位寄存器芯片级联时，通过哪个引脚在芯片之间传递数据？
bitRead()函数读取数据位是从右侧还是左侧开始？
bitSet()函数的功能是什么？
shiftOut()函数有几个参数，各参数的含义是什么？
“〜”和“!”运算符的区别是什么？
位操作符有哪些，各自的功能是什么？
按键消抖有几种分类？
millis()函数的功能是什么？
```





## 4.点阵



![image-20210722114421615](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210722114421615.png)





## 5.串口通信



![image-20210720113232151](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210720113232151.png)

```
1.起始位：在发送数据时，首先会发送一个低电平，表示要开始传输数据了，这个低电平被称为起始位。
2.停止位：总是高电平
3.校验位：用于检验数据传送的正确性，一般分为奇校验和偶校验。传送数据（包含 校验位）中1的个数是奇数，称为奇校验；传送数据中1的个数是偶数，称为偶校验
```

默认为8N1格式，这表示在每个帧中发送8位数据，没有奇偶校验位，停止位只有一位。



初始化函数

```C++
Serial.begin(speed);
Serial.begin(speed,config);
```

### 5.1发送函数

```c++
Serial.print(val);
Serial.print(val,format);
//format：当val为浮点数时,format用于指定输出小数的位数(默认2位)。当 val为非浮点数时.format用于指定输出的进制形式，常用的进制有BIN(二进制)、 OCT(八进制)、DEC(十进制)、HEX(十六进制)。
//加回车换行
Serial.println(val);
Serial, println(val,format);

Serial.write(val);
Serial.write(str);
Serial, write(buf,len);
//返回值:输出的字节数。 val：输出到串口的整型数。 str：输出的字符或字符串。 buf：整数型的数组。
len：缓冲区的长度。
```

### 5.2接收函数

```C++
Serial.available();
//功能:返回UART接收缓冲区中的字节数（能够存储 64 个字节)返回值：缓冲区中可读取的字节数
Serial.read();
//功能:从缓冲区读取数据。每读取1字节，就会从接收缓冲区移除1个字节的数据。返回值：进入串口缓冲区的第一个字节，如果没有可读数据，则返回-1
```

补充函数

```
1.Serial.find(target)
2.Serial.findUnti1(target, terminal)
3.Serial. parseFloat()
4.Serial.parselnt()
5.Serial.peek()
6.Serial.readBytes(buffer, length)

```

设置时间功能的简单报文

![image-20210720113957979](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210720113957979.png)



### EEPROM

EEPROM (Electrically Erasable Programmable read only memory)是指电可擦可编程只读存储器。是一种掉电后数据不丢失的存储芯片。可以理解成我们的硬盘。

ESP32 for Arduino 提供的 EEPROM 读/写类库头文件为 EEPROM. H

使用必须在文件中增加“#include EEPROM.h”

```
1.EEPROM.begin(size)
2.EEPROM.write(addr,data)
3.EEPROM. commit()
4.EEPROM.read(addr)
```

### 蓝牙

1.我们会开启蓝牙。

2.搜索蓝牙设备的名称。

3.建立连接，有一些需要配对密码，有一些则不需要。

4.建立连接之后，我们就会发送数据，或者接收数据。

5.接收方就就解析数据。

接收方收到数据后，有可能是保存数据（如音频文件，视频，图片等），也有可能是命令，（如蓝牙音箱的下一曲）

```
1、BluetoothSerial.begin( localName)；
2、BluetoothSerial.pinCode(pwd)；
3、BluetoothSerial.write(data)；
4、BluetoothSerial.available()；
5、BluetoothSerial.read()；
```



# 程序模块化使用参考



## 1.程序基础代码

```C++
int key = 25;
int led = 26;
void setup(){
    Serial.begin(115200); 
}

void loop(){
    Serial.println("ESP");
}
```



## 2.按键消抖

### 2.1第一种方式

```c++
if(digitalRead(key) == LOW){
      delay(10);
      if(digitalRead(key) == LOW){
        number +=1;
        delay(500);
      }
  }
```

### 2.2第二种方式

```C++
//全局
int preKeyVal = 1;             //保持先前按键值
int preKeyState = 1;           //保持按键状态值
int Keynumber = 0;       //计数
unsigned int preTime = 0;      //保存millis()返回值
int debounceDelay = 10;        //设定消抖时间间隔

void getKeyState(void)
{
  int keyVal = digitalRead(keypin) ;    //读取按键值
  if(keyVal!=preKeyVal)
  {
    preTime = millis();                 //获取当前时间
    preKeyVal = keyVal;                 //将当前值赋值给先前值
  } 
  if((millis() - preTime)>debounceDelay)  
      //判断时间间隔是否大于设定的消抖时间，再判断按键状态是否发生变化
  {
    if(keyVal!=preKeyState)
    {
      preKeyState = keyVal;
      if(keyVal==HIGH)
      {
        Keynumber++; 
      }  
    } 
  }
}
```

## 3.数码管数字数组

实现控制数码管的数组

```
const byte NUM[] = {
  0b11111100,                     //0
  0b01100000,                     //1
  0b11011010,                     //2
  0b11110010,                     //3
  0b01100110,                     //4
  0b10110110,                     //5
  0b10111110,                     //6
  0b11100000,                     //7
  0b11111110,                     //8
  0b11110110,                     //9
  0b00000001                      //.
};
```

### 3.1 数码管显示函数

```C++
void specialDigitDisplay(int digit, int number)
{
  shiftOut(datapin, clockpin, LSBFIRST, ~NUM[number]);
  shiftOut(datapin, clockpin, MSBFIRST, 1<<(digit-1)); //串行移位函数

  digitalWrite(latchpin, HIGH);          //更新数据
  digitalWrite(latchpin, LOW);
}
```



### 3.2.实现精准控制展示时间 1S

```C++
//全局变量
unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 1000;           // interval at which to blink (milliseconds)

//loop循环
unsigned long currentMillis = millis();
if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    number++;
}
```

### 3.3 数码管显示函数  == if else

```C++
switch(String(Keynumber).length()){
    case 1:specialDigitDisplay(1, Keynumber % 10);break;  
    
    case 2:specialDigitDisplay(1, Keynumber % 10);
           specialDigitDisplay(2, Keynumber / 10 % 10);break;  
           
    case 3:specialDigitDisplay(1, Keynumber % 10);
           specialDigitDisplay(2, Keynumber / 10 % 10);
           specialDigitDisplay(3, Keynumber / 100 % 10);break;  
            
    case 4:specialDigitDisplay(1, Keynumber % 10);
           specialDigitDisplay(2, Keynumber / 10 % 10);
           specialDigitDisplay(3, Keynumber / 100 % 10);
           specialDigitDisplay(4, Keynumber / 1000);break;  
  }
```



## 4.点阵（和数码管一样）

0-9

```C++
int heartB[8] = {0x99,0x00,0x00,0x00, 0x81,0xC3,0xE7,0xFF};  //大红 心
int heartS[8] = {0xFF,0xDB,0x81,0x81, 0xC3,0xE7,0xFF,0xFF};  //小红 心
int picJ[8] = {0xEF, 0xC7, 0x83, 0x01, 0xef, 0xef, 0xef, 0xef}; //显示箭头
int picX[8] = {0x3c, 0x18, 0x81, 0xc3, 0xc3, 0x81, 0x18, 0x3c}; //显示X

byte picNum[10][5] = {
        {0x08,0x0A,0x0A,0x0A,0x08},   //0
        {0x0D,0x09,0x0D,0x0D,0x08},   //1
        {0x08,0x0E,0x08,0x0B,0x08},   //2
        {0x08,0x0E,0x08,0x0E,0x08},   //3
        {0x0A,0x0A,0x08,0x0E,0x0E},   //4
        {0x08,0x0B,0x08,0x0E,0x08},   //5
        {0x08,0x0B,0x08,0x0A,0x08},   //6
        {0x08,0x0E,0x0D,0x0D,0x0D},   //7
        {0x08,0x0A,0x08,0x0A,0x08},   //8
        {0x08,0x0A,0x08,0x0E,0x08}    //9
};

//byte picNum[10][8] = {
//  {0x08, 0x0A, 0x0A, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //0
//  {0x0D, 0x09, 0x0D, 0x0D, 0x08, 0x0F, 0x0F, 0x0F}, //1
//  {0x08, 0x0E, 0x08, 0x0B, 0x08, 0x0F, 0x0F, 0x0F}, //2
//  {0x08, 0x0E, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F}, //3
//  {0x0A, 0x0A, 0x08, 0x0E, 0x0E, 0x0F, 0x0F, 0x0F}, //4
//  {0x08, 0x0B, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F}, //5
//  {0x08, 0x0B, 0x08, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //6
//  {0x08, 0x0E, 0x0D, 0x0D, 0x0D, 0x0F, 0x0F, 0x0F}, //7
//  {0x08, 0x0A, 0x08, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //8
//  {0x08, 0x0A, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F} //9
//};
```



点阵函数

```C++
void matrixDisplay()
{
  shiftOut(datapin,clockpin,MSBFIRST,dataCol);      //串行移位
  shiftOut(datapin,clockpin,LSBFIRST,dataRow);      //串行移位
  digitalWrite(latchpin,HIGH);
  digitalWrite(latchpin,LOW); 
}
```



点亮点阵流水灯

```C++
//loop
for(int r=0;r<8;r++)
  {
    dataRow = 1<<r;     //共阳点阵行
    for(int c=0;c<8;c++)
    {
      dataCol = ~(1<<c);   //共阳点阵列
      matrixDisplay();
      delay(200);
    }
  }
```





## 5.串口

接收处理

```C++
//loop
if (Serial.available() > 0) // 串口收到字符数大于零(获得字符串)。
{
	comdata += char(Serial.read());  //累加获得的值
}
Serial.println(comdata);//打出字符char
Serial.println(comdata.toInt());//打出数值int
```



## 2020 9

```
//////实现计数功能1-100  采用精准时间+串口控制延时
//2020年9月真题
const int datapin = 25;         //DS
const int clockpin = 27;        //SH_CP SCK
const int latchpin = 26;         //ST_CP RCK

unsigned long previousMillis = 0;        // will store last time LED was updated
long interval = 1000;           // interval at which to blink (milliseconds)
String comdata = "";
const byte NUM[] = {
  0b11111100,                     //0
  0b01100000,                     //1
  0b11011010,                     //2
  0b11110010,                     //3
  0b01100110,                     //4
  0b10110110,                     //5
  0b10111110,                     //6
  0b11100000,                     //7
  0b11111110,                     //8
  0b11110110,                     //9
  0b00000001                      //.
};
int number = 0;
void specialDigitDisplay(int digit, int number)
{
  byte val = 0;
  val = bitSet(val, digit - 1);      //将digit对应位号置位
  shiftOut(datapin, clockpin, LSBFIRST, ~NUM[number]);
  shiftOut(datapin, clockpin, MSBFIRST, val); //串行移位函数

  digitalWrite(latchpin, HIGH);          //更新数据
  digitalWrite(latchpin, LOW);
  //  delay(500);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(datapin, OUTPUT);
  pinMode(clockpin, OUTPUT);
  pinMode(latchpin, OUTPUT);        //配置为输出模式
  digitalWrite(clockpin, LOW);      //设置为低电平状态
  digitalWrite(latchpin, LOW);      //设置为低电平状态
}
void loop() {
  if (Serial.available() > 0) // 串口收到字符数大于零(获得字符串)。
  {
    comdata += char(Serial.read());
  }
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    number++;
    // Serial.println(String(number).length());
   if(comdata != ""){
        Serial.println(comdata.toInt());//打出数值
        interval = comdata.toInt();
        comdata = "";
    }
  }
  switch (String(number).length()) {
    case 1: specialDigitDisplay(1, number % 10); break;

    case 2: specialDigitDisplay(1, number % 10);
      specialDigitDisplay(2, number / 10 % 10); break;

    case 3: specialDigitDisplay(1, number % 10);
      specialDigitDisplay(2, number / 10 % 10);
      specialDigitDisplay(3, number / 100 % 10); break;

    case 4: specialDigitDisplay(1, number % 10);
      specialDigitDisplay(2, number / 10 % 10);
      specialDigitDisplay(3, number / 100 % 10);
      specialDigitDisplay(4, number / 1000); break;
  }
}
```



## 2020 12

```
//实现大红心和小红心切换  使用电位器控制切换频率 数码管显示切换时间
//控制点阵管脚
const int datapin = 23;           //DS   串行数据输入
const int clockpin = 18;          //SH_CP //串行时钟输入 上升沿移位
const int latchpin = 5;           //ST_CP  //锁存 高电平存储
//控制数码管管脚
const int datapin1 = 25;         //DS
const int clockpin1 = 27;        //SH_CP
const int latchpin1 = 26;         //ST_CP

byte dataRow;        //行R1~R8
byte dataCol;        //列数据C1~C8

int heartB[8] = {0x99, 0x00, 0x00, 0x00, 0x81, 0xC3, 0xE7, 0xFF}; //大红 心
int heartS[8] = {0xFF, 0xDB, 0x81, 0x81, 0xC3, 0xE7, 0xFF, 0xFF}; //小红 心
//精准定时初始化
unsigned long  oldTime  = 0;
const byte NUM[] = {
  0b11111100,                     //0
  0b01100000,                     //1
  0b11011010,                     //2
  0b11110010,                     //3
  0b01100110,                     //4
  0b10110110,                     //5
  0b10111110,                     //6
  0b11100000,                     //7
  0b11111110,                     //8
  0b11110110,                     //9
  0b00000001                      //.
};

bool flag = 0;
void matrixDisplay()
{
  shiftOut(datapin, clockpin, MSBFIRST, dataCol);   //串行移位
  shiftOut(datapin, clockpin, LSBFIRST, dataRow);   //串行移位
  digitalWrite(latchpin, HIGH);
  digitalWrite(latchpin, LOW);
}
void specialDigitDisplay(int digit, int number)
{
  shiftOut(datapin1, clockpin1, LSBFIRST, ~NUM[number]);
  shiftOut(datapin1, clockpin1, MSBFIRST, 1 << (digit - 1)); //串行移位函数

  digitalWrite(latchpin1, HIGH);          //更新数据
  digitalWrite(latchpin1, LOW);
}
void setup() {
  //点阵
  pinMode(datapin, OUTPUT);
  pinMode(clockpin, OUTPUT);
  pinMode(latchpin, OUTPUT);            //设置为输出
  digitalWrite(clockpin, LOW);          //0
  digitalWrite(latchpin, LOW);          //0
  //数码管
  pinMode(datapin1, OUTPUT);
  pinMode(clockpin1, OUTPUT);
  pinMode(latchpin1, OUTPUT);            //设置为输出
  digitalWrite(clockpin1, LOW);          //0
  digitalWrite(latchpin1, LOW);          //0

  pinMode(16, INPUT);
  Serial.begin(9600);
}
int Ctime = 500;
int keyVal;
void loop() {
  //利用函数millis()和变量oldTime来计时
  //设定切换时间为间隔1s，400ms
  keyVal = analogRead(A6);
  keyVal = map(keyVal, 0, 4095, 100, 500);
  if (millis() - oldTime >= keyVal)
  {
    oldTime = millis();          //当前时间，暂存oldTime
    flag = !flag;
    Serial.println(keyVal);
  }
  if (keyVal == 500) {
    return;
  }
  for (int i = 0; i < 8; i++) {
    dataRow = 1 << i;
    if (flag) {
      dataCol = heartB[i];
    } else {
      dataCol = heartS[i];
    }
    matrixDisplay();
  }
  specialDigitDisplay(1, keyVal % 10);
  specialDigitDisplay(2, keyVal / 10 % 10);
  specialDigitDisplay(3, keyVal / 100 % 10); 
}

```





综合例程

```C++
//点阵数码管红绿灯  当为箭头的时候绿灯亮  若为红灯 红灯亮  数码管显示倒计时30s切换
//控制点阵管脚
const int datapin = 23;           //DS   串行数据输入
const int clockpin = 18;          //SH_CP //串行时钟输入 上升沿移位
const int latchpin = 5;           //ST_CP  //锁存 高电平存储
//控制数码管管脚
const int datapin1 = 25;         //DS
const int clockpin1 = 27;        //SH_CP
const int latchpin1 = 26;         //ST_CP

int picJ[8] = {0xEF, 0xC7, 0x83, 0x01, 0xef, 0xef, 0xef, 0xef}; //显示箭头

int picX[8] = {0x3c, 0x18, 0x81, 0xc3, 0xc3, 0x81, 0x18, 0x3c}; //显示X

//精准定时初始化
unsigned long  oldTime  = 0;
bool flag = 0;
byte dataRow;        //行R1~R8
byte dataCol;        //列数据C1~C8

int Ctime = 30;
byte picNum[10][8] = {
  {0x08, 0x0A, 0x0A, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //0
  {0x0D, 0x09, 0x0D, 0x0D, 0x08, 0x0F, 0x0F, 0x0F}, //1
  {0x08, 0x0E, 0x08, 0x0B, 0x08, 0x0F, 0x0F, 0x0F}, //2
  {0x08, 0x0E, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F}, //3
  {0x0A, 0x0A, 0x08, 0x0E, 0x0E, 0x0F, 0x0F, 0x0F}, //4
  {0x08, 0x0B, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F}, //5
  {0x08, 0x0B, 0x08, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //6
  {0x08, 0x0E, 0x0D, 0x0D, 0x0D, 0x0F, 0x0F, 0x0F}, //7
  {0x08, 0x0A, 0x08, 0x0A, 0x08, 0x0F, 0x0F, 0x0F}, //8
  {0x08, 0x0A, 0x08, 0x0E, 0x08, 0x0F, 0x0F, 0x0F} //9
};
const byte NUM[] = {
  0b11111100,                     //0
  0b01100000,                     //1
  0b11011010,                     //2
  0b11110010,                     //3
  0b01100110,                     //4
  0b10110110,                     //5
  0b10111110,                     //6
  0b11100000,                     //7
  0b11111110,                     //8
  0b11110110,                     //9
  0b00000001                      //.
};
//控制点阵
void matrixDisplay()
{
  shiftOut(datapin, clockpin, MSBFIRST, dataCol);   //串行移位
  shiftOut(datapin, clockpin, LSBFIRST, dataRow);   //串行移位
  digitalWrite(latchpin, HIGH);
  digitalWrite(latchpin, LOW);                      //下降沿，数据送出
}
//控制数码管
void specialDigitDisplay(int digit, int number)
{
  shiftOut(datapin1, clockpin1, LSBFIRST, ~NUM[number]);
  shiftOut(datapin1, clockpin1, MSBFIRST, 1 << (digit - 1)); //串行移位函数

  digitalWrite(latchpin1, HIGH);          //更新数据
  digitalWrite(latchpin1, LOW);
}
void setup() {
  //点阵
  pinMode(datapin, OUTPUT);
  pinMode(clockpin, OUTPUT);
  pinMode(latchpin, OUTPUT);            //设置为输出
  digitalWrite(clockpin, LOW);          //0
  digitalWrite(latchpin, LOW);          //0
  //数码管
  pinMode(datapin1, OUTPUT);
  pinMode(clockpin1, OUTPUT);
  pinMode(latchpin1, OUTPUT);            //设置为输出
  digitalWrite(clockpin1, LOW);          //0
  digitalWrite(latchpin1, LOW);          //0

  pinMode(16,OUTPUT);
  pinMode(17,OUTPUT);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  //利用函数millis()和变量oldTime来计时
  //设定切换时间为间隔1s，400ms
  if (millis() - oldTime >= 1000)
  {
    oldTime = millis();          //当前时间，暂存oldTime
    Ctime --;
  }
  if (Ctime == -1) {
    Ctime = 30;
    flag = !flag;
  }
  //显示切换时间
  specialDigitDisplay(1, Ctime % 10); 
  specialDigitDisplay(2, Ctime / 10);

  for (int i = 0; i < 8; i++)
  {
    dataRow = 1 << i;
    if (flag) {
      dataCol = picX[i];
      digitalWrite(16,HIGH);
      digitalWrite(17,LOW);
    } else {
      dataCol = picJ[i];
      digitalWrite(16,LOW);
      digitalWrite(17,HIGH);
    }

    matrixDisplay();
  }
}
```


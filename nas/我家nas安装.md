



## 一、爱快

![image-20210717083846653](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717083846653.png)

### 内外网

![image-20210717084115542](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084115542.png)

### DHCP

![image-20210717084044520](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084044520.png)



## 二、lede

### 接口

接下来三张图都在lan里配置  先选择lan接口，如第三张图  其次配置第二张，其次强制

![image-20210717084226670](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084226670.png)



![image-20210717084415274](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084415274.png)

![](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084245892.png)



![image-20210717084011925](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717084011925.png)



## 三、群晖

![image-20210717083929376](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210717083929376.png)











## 四、kodi

```
apt-get update  //检测更新
//安装所需程序：
apt-get install xorg xinit xfce4 lightdm kodi kodi-repository-kodi i965-va-driver -y 
//创建kodi用户
useradd -m kodi -s /bin/bash -d /home/kodi
//修改文件
/etc/lightdm/lightdm.conf
```

![image-20210718125825514](https://zhanghaooss.oss-cn-beijing.aliyuncs.com/blogImg/image-20210718125825514.png)

修改lightdm.conf

![image-20210718125904443](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210718125904443.png)

**中文问题，去ADD-ONS里面装中文的语言**

Kodi显示乱码是因为字体不正确导致。在【**Skin**】皮肤设置里面找到【**Fonts**】字体设置，将【**SKin default**】默认字体改成【**Arial based】**字体就可以解决了

**解决Kodi播放无声问题**

```
apt-get install alsa-untils //安装alsa工具包
alsamixer 进入音量控制工具
```

参考  https://post.smzdm.com/p/ag827pzw/

​		https://www.bilibili.com/video/BV1Ge41147B9



便试着按下了F9(减小音量) F10(增大音量)，问题解决了



在PVE安装的时候硬盘选项

```
PVE的安装程序会创建一个名为“pve”的卷组（VG），并在其上分别创建名为root、data和swap的逻辑卷（LV）。这些卷的大小可以通过以下方式控制：

安装程序创建一个名为pve的卷组（VG），以及名为root，data和swap的其他逻辑卷（LV）。这些卷的大小可以通过设置参数来控制大小：

hdsize：定义要使用的硬盘（HD）的总体大小。在安装时可以预留一部分的磁盘空间，这样可节省硬盘上的可用空间做进一步分区（例如可用于LVM存储的同一硬盘上另外的PV和VG）。
swapsize：定义交换空间（swap）的大小。默认值是已安装内存的大小，最小为4 GB，最大为8 GB。其最大限制值不能为“hdsize”的八分之一。
maxroot：定义存储操作系统的根卷（/root）的最大大小。其最大限制为“hdsize”的四分之一。
maxvz：定义数据卷（data）的最大大小。其实际大小可以按照下面方式来计算：
datasize = hdsize - rootsize - swapsize - minfree
minfree：定义划分LVM卷组“pve”后剩余的可用空间量。当可用存储空间超过128GB时，其默认值为16GB，否则将使用“hdsize”的八分之一。
需要注意的是，LVM需要VG中的可用空间来创建快照，而lvmthin快照则不需要
```

```
111 hdsize
5  swapsize
60 maxroot
40 maxvz
30 minfree
```

1.free 查看剩余空间

```
free
```

![image-20210716215150441](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210716215150441.png)

2.对local对应的逻辑卷pve/root进行在线扩容

\# lvextend -l +100%FREE -r pve/root






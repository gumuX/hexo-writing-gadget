package otherFile

//import (
//	"fmt"
//	"os"
//	"path/filepath"
//	"strings"
//)
//
//func check(e error) {
//	if e != nil {
//		panic(e)
//	}
//}
//
///**
// * 判断文件是否存在  存在返回 true 不存在返回false
// */
//func checkFileIsExist(filename string) bool {
//	var exist = true
//	if _, err := os.Stat(filename); os.IsNotExist(err) {
//		exist = false
//	}
//	return exist
//}
//var listfile []string //获取md文件列表
//var upname []string //获取当前md文件上级名称
//func listfunc(path string, f os.FileInfo, err error) error {
//	var strRet string
//	strRet, _ = os.Getwd()
//	strRet += "\\"
//	if f == nil {
//		return err
//	}
//	strRet += path //+ "\r\n"
//
//	if f.IsDir() {//如果是目录
//		if !strings.Contains(path,".") { //目录不包含./
//			fmt.Print(strRet+"->")
//			fmt.Println(f.Name()) //打印文件或目录名
//			upname = append(upname,path)
//			filepathNames, _ := filepath.Glob(filepath.Join(strRet, "*"))
//			for i := range filepathNames {
//					fmt.Println(filepathNames[i]) //打印path
//				}
//				fmt.Println("----------------------------------------")
//		}
//		return nil
//	}
//	//用strings.HasSuffix(src, suffix)//判断src中是否包含 suffix结尾
//	ok := strings.HasSuffix(strRet, ".md")
//	if ok {
//		listfile = append(listfile, strRet) //将目录push到listfile []string中
//	}
//	//fmt.Println(strRet) //list the file
//
//	return nil
//}
//func getFileList(path string) string {
//	//var strRet string
//	err := filepath.Walk(path, listfunc) //
//
//	if err != nil {
//		fmt.Printf("filepath.Walk() returned %v\n", err)
//	}
//
//	return " "
//}
//
//func ListFileFunc(p []string) {
//	for index, value := range p {
//		fmt.Println("Index = ", index, "Value = ", value)
//	}
//}
//func GetUpMdFileName()  {
//
//	//pwd, _ := os.Getwd()
//	//获取文件或目录相关信息
//	//fileInfoList, err := ioutil.ReadDir(pwd)
//	//if err != nil {
//	//	log.Fatal(err)
//	//}
//	//fmt.Println(len(fileInfoList))
//	//for i := range fileInfoList {
//	//	fmt.Println(fileInfoList[i].Name()) //打印当前文件或目录下的文件或目录名
//	//}
//	//获取当前目录下的文件或目录名(包含路径)
//	//filepathNames, err := filepath.Glob(filepath.Join(pwd, "*"))
//	//if err != nil {
//	//	log.Fatal(err)
//	//}
//	//for i := range filepathNames {
//	//	fmt.Println(filepathNames[i]) //打印path
//	//}
//	//获取当前目录下的所有文件或目录信息
//	//filepath.Walk(pwd, func(path string, info os.FileInfo, err error) error {
//	//	fmt.Println(path)        //打印path信息
//	//	fmt.Println(info.Name()) //打印文件或目录名
//	//	return nil
//	//})
//}
//func main() {
//	//reg1 := regexp.MustCompile(`\*.md`)
//
//	getFileList("./")  //获得
//	ListFileFunc(listfile)
//	GetUpMdFileName()
//	//GetUpMdFileName(str5)
//	//for i, i2 := range listfile {
//	//	fmt.Print(i)
//	//	fmt.Println(i2)
//	//}
//	//var wireteString = "测试n123"
//	//var filename = "./*.md"
//	//var f *os.File
//	//var err1 error
//	///***************************** 第一种方式: 使用 io.WriteString 写入文件 ***********************************************/
//	//if checkFileIsExist(filename) { //如果文件存在
//	//	f, err1 = os.OpenFile(filename, os.O_APPEND, 0666) //打开文件
//	//	fmt.Println("文件存在")
//	//} else {
//	//	f, err1 = os.Create(filename) //创建文件
//	//	fmt.Println("文件不存在")
//	//}
//	//check(err1)
//	//n, err1 := io.WriteString(f, wireteString) //写入文件(字符串)
//	//check(err1)
//	//fmt.Printf("写入 %d 个字节n", n)
//	//
//	///*****************************  第二种方式: 使用 ioutil.WriteFile 写入文件 ***********************************************/
//	//var d1 = []byte(wireteString)
//	//err2 := ioutil.WriteFile("./output2.txt", d1, 0666) //写入文件(字节数组)
//	//check(err2)
//	//
//	///*****************************  第三种方式:  使用 File(Write,WriteString) 写入文件 ***********************************************/
//	//f, err3 := os.Create("./output3.txt") //创建文件
//	//check(err3)
//	//defer f.Close()
//	//n2, err3 := f.Write(d1) //写入文件(字节数组)
//	//check(err3)
//	//fmt.Printf("写入 %d 个字节n", n2)
//	//n3, err3 := f.WriteString("writesn") //写入文件(字节数组)
//	//fmt.Printf("写入 %d 个字节n", n3)
//	//f.Sync()
//	//
//	///***************************** 第四种方式:  使用 bufio.NewWriter 写入文件 ***********************************************/
//	//w := bufio.NewWriter(f) //创建新的 Writer 对象
//	//n4, err3 := w.WriteString("bufferedn")
//	//fmt.Printf("写入 %d 个字节n", n4)
//	//w.Flush()
//	//f.Close()
//}
//
//
//
///*
//
// //* Golang语言字符串处理函数包strings，字符串类型转换包strconv
//
//package main
//
//import (
//"fmt"
//"strconv"
//"strings"
//)
//
//func main() {
//	str := "This is an example of a string"
//	fmt.Printf("Ture/False? Does the string "%s" have prefix %s?", str, "Th")
//	// 判断字符串的前缀
//	fmt.Printf("%tn", strings.HasPrefix(str, "Th"))
//	fmt.Printf("True/False? Does the string "%s" have suffix %s?", str, "ing")
//	// 判断字符串的后缀
//	fmt.Printf("%tn", strings.HasSuffix(str, "ing"))
//	fmt.Printf("True/False? Does the string "%s" have %s?", str, "xa")
//	// 判断字符串是否包含某字符
//	fmt.Printf("%tn", strings.Contains(str, "xa"))
//	// 判断指定字符在字符串第一次出现的位置
//	fmt.Printf("%dn", strings.Index(str, "s"))
//	// 判断指定字符在字符串最后一次出现的位置
//	fmt.Printf("%dn", strings.LastIndex(str, "s"))
//	// 将字符串中的前n个字符替换，并返回一个新字符串，如果n=-1则替换所有字符串中的字符
//	fmt.Printf("%sn", strings.Replace(str, "is", "at", 1))
//	// 统计指定字符在字符串中出现的次数
//	fmt.Printf("%dn", strings.Count(str, "s"))
//	// 将字符串重复n次，并返回新字符串
//	fmt.Printf("%sn", strings.Repeat(str, 2))
//	// 将字符串全部转换为小写字符
//	fmt.Printf("%sn", strings.ToLower(str))
//	// 将字符串全部转换为大写字符
//	fmt.Printf("%sn", strings.ToUpper(str))
//	// 去除字符串开头和结尾的空格
//	str1 := " This is an example of a string "
//	fmt.Printf("%sn", strings.TrimSpace(str1))
//	// 去除字符串开头和结尾的指定字符，只去除字符串开头或结尾的指定字符
//	str2 := "my name is frank, this pencil is my"
//	fmt.Printf("%sn", strings.Trim(str2, "my"))
//	fmt.Printf("%sn", strings.TrimLeft(str2, "my"))
//	fmt.Printf("%sn", strings.TrimRight(str2, "my"))
//	// 分割字符串，转换为一个slice
//	// 利用1个或多个空白字符来作为分隔符，将字符串分割成若干小块，并返回一个slice
//	// 如果字符串只包含空白字符串，则返回一个长度为0的slice
//	sli := strings.Fields(str)
//	for _, value := range sli {
//		fmt.Println(value)
//	}
//	str3 := "2019-05-07"
//	sli2 := strings.Split(str3, "-")
//	for _, value := range sli2 {
//		fmt.Println(value)
//	}
//	// 拼接slice元素成为一个字符串
//	fmt.Printf("%sn", strings.Join(sli2, "-"))
//	// 字符串类型转换
//	str4 := "123"
//	// 字符串类型转换为int
//	num, _ := strconv.Atoi(str4)
//	fmt.Printf("%dn", num)
//	// int转换为字符串类型
//	newStr4 := strconv.Itoa(num)
//	fmt.Printf("%sn", newStr4)
//}
//*/

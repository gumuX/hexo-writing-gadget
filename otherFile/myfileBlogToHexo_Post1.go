package otherFile

import (
	"bufio"
	"fmt"
	"github.com/spf13/viper"
	"io"
	"os"
	"path/filepath"
	"strings"
	"syscall"
)

//Tolocalation
//var moveLocalation ="D:\\hexoBlog\\source\\_posts\\"

func movefile(oldpath, newpath string) error { //跨卷移动os.rename 无法跨卷
	from, err := syscall.UTF16PtrFromString(oldpath)
	if err != nil {
		return err
	}
	to, err := syscall.UTF16PtrFromString(newpath)
	if err != nil {
		return err
	}
	return syscall.MoveFile(from, to) //windows API

}

//插入或者替换文件的某一行InUp 为 0 插入  1 替换
func fileInsertInfo(linenum int, linecontent string, filename string, filenamesub string, InUp int) {
	// 打开要操作的文件 os.O_RDWR: 可读可写
	file, err := os.OpenFile(filename, os.O_RDWR, 0544)
	if err != nil {
		fmt.Printf("File open failed! err: %v\n", err)
		return
	}
	reader := bufio.NewReader(file)

	// 新建临时文件
	tempFile, err := os.OpenFile("./hello.md", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Printf("Temp create failed! err: %v\n", err)
		return
	}
	writer := bufio.NewWriter(tempFile)
	_ = writer.Flush()
	for i := 0; i < linenum; i++ {
		// 在第n行插入新内容
		// 将原文件写入临时文件
		line, err := reader.ReadString('\n') // 依次读一行
		//fmt.Print(line)
		if err != nil {
			fmt.Printf("Read file failed! err: %v\n", err)
			return
		}
		if i < (linenum-1) || InUp == 0 {
			// 写入临时文件
			_, _ = writer.WriteString(line)
			_ = writer.Flush()
			// 移动光标
			//_, _ = file.Seek(int64(len(line)), 0)
			// 写入要插入的内容
		}
	}
	var tempInfo string
	tempInfo = linecontent
	_, _ = tempFile.WriteString(tempInfo)
	_ = writer.Flush()
	// 把源文件的后续内容写入临时文件
	for {
		line, err := reader.ReadString('\n') // 依次读一行
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("File raed failed! err: %v\n", err)
			return
		}
		_, _ = writer.WriteString(line)
	}
	_ = writer.Flush()

	file.Close()
	tempFile.Close()
	//如果文件包含# 则不移动
	if strings.Contains(filenamesub, "#") {
		return
	}
	strblog := moveLocalation + filenamesub + ".md"
	os.Remove(strblog)                    //删除文件 然后重新创建
	err = movefile("./hello.md", strblog) //重命名  并移动 跨卷
	//err = os.Rename("./hello.md", "G:\\"+filenamesub+".md")//重命名  并移动 不跨卷
	if err != nil {
		fmt.Printf("Rename file raed failed! err: %v\n", err)
		return
	}
}

var listfile []string //获取md文件列表
var upname []string   //获取当前md文件上级名称
func listfunc(path string, f os.FileInfo, err error) error {
	var strRet string
	strRet, _ = os.Getwd()
	strRet += "\\"
	if f == nil {
		return err
	}
	strRet += path //+ "\r\n"

	if f.IsDir() { //如果是目录
		if !strings.Contains(path, ".") { //目录不包含./
			fmt.Print(strRet + "->")
			fmt.Println(f.Name()) //打印文件或目录名
			upname = append(upname, path)
			filepathNames, _ := filepath.Glob(filepath.Join(strRet, "*"))
			for i := range filepathNames {
				if strings.Contains(filepathNames[i], "#") { //如果目录包含# 则排除
					continue
				}
				fmt.Print(filepathNames[i] + "->") //打印path
				fmt.Println(filepathNames[i][len(strRet)+1 : len(filepathNames[i])-3])
				//fileInsertInfo(0+1,,filepathNames[i],1)
				//fileInsertInfo(1+1,"title: "+f.Name()+"->"+filepathNames[i][len(strRet)+1:len(filepathNames[i])-3]+"\n",filepathNames[i],1)
				//fileInsertInfo(2+1,"top: ",filepathNames[i],1)   //date
				str := "---\n" +
					"title: " + filepathNames[i][len(strRet)+1:len(filepathNames[i])-3] + "\n" +
					"top: \n" +
					"abbrlink: \n" +
					"password: \n" +
					"abstract: \n" +
					"message: \n" +
					"categories: " + f.Name() + "\n" +
					"tags: null\n" +
					"---\n"
				if strings.Contains(filepathNames[i], ".md") { //如果 不是md文件则排除
					fileInsertInfo(0, str, filepathNames[i], filepathNames[i][len(strRet)+1:len(filepathNames[i])-3], 1)
				}

			}
			fmt.Println("----------------------------------------")
		}
		return nil
	}
	//用strings.HasSuffix(src, suffix)//判断src中是否包含 suffix结尾
	ok := strings.HasSuffix(strRet, ".md")
	if ok {
		listfile = append(listfile, strRet) //将目录push到listfile []string中
	}
	//fmt.Println(strRet) //list the file

	return nil
}
func getFileList(path string) string {
	//var strRet string
	err := filepath.Walk(path, listfunc) //

	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}

	return " "
}

var moveLocalation string

func main() {
	InitConfig()
	moveLocalation = viper.GetString("Tolocation")
	fmt.Println(moveLocalation)
	getFileList("./")
	//插入或者替换文件的某一行InUp 为 0 插入  1 替换
	//fileInsertInfo(14+1,"hi hello\n","./test.md",0)
}
func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

//参考链接  https://blog.csdn.net/qq_44733706/article/details/113143691

package main

import (
	"bufio"
	"fmt"
	"github.com/spf13/viper"
	"io"
	"os"
	"strings"
	"syscall"
)

func movefile(oldpath, newpath string) error { //跨卷移动os.rename 无法跨卷
	from, err := syscall.UTF16PtrFromString(oldpath)
	if err != nil {
		return err
	}
	to, err := syscall.UTF16PtrFromString(newpath)
	if err != nil {
		return err
	}
	return syscall.MoveFile(from, to) //windows API

}

func FileCreateInsertAndMove(filepath string, filename string, mulu []string, Date string, Tag string, Top string, countline int) {
	fmt.Print(filepath + "--->" + filename + "---->" + Date + "|" + Tag + "|" + Top + "-->目录:")
	var cate string
	for i := 1; i < len(mulu)-1; i++ {
		cate += fmt.Sprintf("  - " + mulu[i] + "\n")
	}
	fmt.Println()
	str := "---\n" +
		"title: " + filename + "\n" +
		Top + "\n" +
		"abbrlink: \n" +
		"password: \n" +
		"abstract: \n" +
		"message: \n" +
		"categories: " + "\n" + cate + "\n" +
		Tag + "\n" +
		Date + "\n" +
		"---\n"
	if strings.Contains(filepath, "#") || strings.Contains(filename, "#") { //如果文件包含#号 排除该文件
		return
	}
	//打开文件  读取内容
	fp, err := os.OpenFile(filepath, os.O_RDONLY, 0666)
	if err != nil {
		fmt.Println("文件打开失败", err)
	}
	//reader := bufio.NewReader(fp)
	//新建临时文件 写入内容
	tempFile, err := os.OpenFile("./hello.md", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)

	if err != nil {
		fmt.Printf("Temp create failed! err: %v\n", err)
		return
	}
	writer := bufio.NewWriter(tempFile)
	_, _ = writer.WriteString(str)
	writer.Flush()
	reader := bufio.NewReader(fp)
	i := 0
	for {
		i++
		line, err := reader.ReadString('\n') // 依次读一行
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("File raed failed! err: %v\n", err)
			return
		}
		if i > countline { //将前三行去掉不读
			_, _ = writer.WriteString(line)
		}
		writer.Flush()
	}
	fp.Close()
	tempFile.Close()

	strblog := viper.GetString("Geolocation") + filename + ".md"
	err = os.Remove(strblog)
	if err != nil {
		fmt.Println(err)
	} //删除文件 然后重新创建
	err = movefile("./hello.md", strblog) //重命名  并移动 跨卷
	if err != nil {
		fmt.Printf("Rename file raed failed! err: %v\n", err)
		return
	}
}

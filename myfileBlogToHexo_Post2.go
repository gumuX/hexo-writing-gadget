package main

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

func getFileList(path string) string {
	//var strRet string
	err := filepath.Walk(path, Listfunc) //

	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}

	return " "
}

var moveLocalation string

func main() {
	InitConfig()
	moveLocalation = viper.GetString("Geolocation")
	getFileList("./")
}
func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

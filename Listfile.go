package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var WorkDir, _ = os.Getwd()

func ReadLineDate(filepath string) (string, string, string) { //date tags  top
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0666)
	//及时关闭file句柄
	defer file.Close()
	if err != nil {
		fmt.Println("文件打开失败", err)
	}
	//写入文件时，使用带缓存的 *Writer
	reader := bufio.NewReader(file)
	line1, err := reader.ReadBytes('\n') //读取日期
	line2, err := reader.ReadBytes('\n') //读取标签
	line3, err := reader.ReadBytes('\n') //读取置顶选项
	if err != nil {
		panic(err)
	}
	return string(line1), string(line2), string(line3)
}
func Listfunc(path string, f os.FileInfo, err error) error {
	var strRet string
	strRet, _ = os.Getwd()
	strRet += "\\"
	if f == nil {
		return err
	}
	strRet += path                                 //+ "\r\n"
	if f.IsDir() && !strings.Contains(path, ".") { //目录不包含.
		//fmt.Println(WorkDir)
		fmt.Print(strRet + "->") //打印文件路径
		fmt.Println(f.Name())    //打印目录名
		//比如  D:\Project\ProjectGo\filelean\ESP32\单片机->单片机
		filepathNames, _ := filepath.Glob(filepath.Join(strRet, "*"))
		for i := range filepathNames {
			if strings.Contains(filepathNames[i], ".md") && !strings.Contains(filepathNames[i], "#") {
				var filename = filepathNames[i][len(strRet)+1 : len(filepathNames[i])-3]
				//fmt.Print(filepathNames[i] + "------>"+filename+"------目录:") //打印文件路径和文件名称
				//D:\Project\ProjectGo\filelean\GO笔记\学习blog\1.Go环境配置.md------>1.Go环境配置------目录: GO笔记 学习blog
				mulu := strings.Split(filepathNames[i][len(WorkDir):len(filepathNames[i])-len(filename)-3], "\\")
				for i := 0; i < len(mulu); i++ {
					//fmt.Print(mulu[i]+" ")
				}
				countline := 0
				ReadFileDate, ReadFileTag, ReadFileTop := ReadLineDate(filepathNames[i])
				if strings.Contains(ReadFileDate, "date") { //根据文件读取第一行Date日期
					ReadFileDate = ReadFileDate[1 : len(ReadFileDate)-2]
					countline++
				} else {
					ReadFileDate = ""
				}
				if strings.Contains(ReadFileTag, "tags") {
					ReadFileTag = ReadFileTag[1 : len(ReadFileTag)-2]
					countline++
				} else {
					ReadFileTag = ""
				}
				if strings.Contains(ReadFileTop, "top") {
					ReadFileTop = ReadFileTop[1 : len(ReadFileTop)-2]
					countline++
				} else {
					ReadFileTop = ""
				}
				//获得所有参数后开始将数据插入到新建的md文件  并放到post下
				FileCreateInsertAndMove(filepathNames[i], filename, mulu, ReadFileDate, ReadFileTag, ReadFileTop, countline)

				fmt.Println()

			}
		}
		fmt.Println("--------------------------------")
	}

	return nil
}

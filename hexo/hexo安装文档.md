date: 2021-07-23 11:41:48

文章模板

```
---
title: ESP32课件
top: 
password: 
categories:
  - 单片机
tags:
  - null
abbrlink: a642292e
date: 2021-07-23 11:41:48
---
```

## 1.安装

```
git clone https://github.com/yelog/hexo-theme-3-hexo.git themes/3-hexo
```

### 1.1更新

```
cd themes/3-hexo
git pull
```

### 1.2禁用 hexo 自带的高亮

```
highlight:
enable: false
```

更多参考

https://yelog.org/2017/03/23/3-hexo-instruction/

## 2.实现私密文章

### 2.1安装插件

```
npm install hexo-blog-encrypt --save
```

### 2.2主配置文件`MyWeb\_config.yml`文末添加

```
# 文章加密
encrypt:
  enable: true
  abstract: 这是一篇加密文章，内容可能是个人情感宣泄或者收费技术。如果你非常好奇，请与我联系。
  message: 输入密码，查看文章。
```

### 2.3文章Front-matter中加上`password: 123456`即可：

```
---
title: Hello World
abbrlink: 3eeb
password: 123456
---
```

自定义提示语：

```
---
title: Hello World
abbrlink: 3eeb
password: 123456
abstract: 密码：123456
message: 看不到吧，hhhh，不告诉你密码是123456
---
```

如果需要加密其他东西

```
在任何需要加密的地方加上一句：
<% if (post.encrypt == true) { %>style="display:none" <% } %>
```

## 3.添加Tags

修改 3-hexo/source/js/script.js文件

修改 3-hexo/source/css/_partial/nav-left.styl  第16行  margin 30px auto 15px

在366行左右添加一个else if

```js
else if(val === "#"){
		$(".nav-right nav a").css("display", "none");
		$(".nav-right .tags-list").show()
	}
```

## 4.实现永久链接化 

安装  https://github.com/Rozbo/hexo-abbrlink

```
npm install hexo-abbrlink --save
```

站点配置文件(_config.yml)里:  注意有没有重复定义permalink

```
# abbrlink config
permalink: posts/:abbrlink/
abbrlink:
  alg: crc32      #support crc16(default) and crc32
  rep: hex        #support dec(default) and hex
  drafts: false   #(true)Process draft,(false)Do not process draft. false(default) 
  # Generate categories from directory-tree
  # depth: the max_depth of directory-tree you want to generate, should > 0
  auto_category:
     enable: true  #true(default)
     depth:        #3(default)
     over_write: false 
  auto_title: false #enable auto title, it can auto fill the title by path
  auto_date: false #enable auto date, it can auto fill the date by time today
  force: false #enable force mode,in this mode, the plugin will ignore the cache, and calc the abbrlink for every post even it already had abbrlink.
```

使用`hexo g` 会自动在你的文章中加上`abbrlink: fbf5310d`









部署  gumugwen.github.io

参考链接

https://cloud.tencent.com/developer/article/1755885

https://www.cnblogs.com/liuxianan/p/build-blog-website-by-hexo-github.html



## 运行

配置package.json

```go
"scripts": {
    "build": "hexo generate",
    "clean": "hexo clean",
    "deploy": "hexo clean && hexo g -d",
    "server": "hexo server",
	"start": "hexo clean && hexo g && hexo s"
  }
```



如果写完一个blog的时候

![image-20210728213604231](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210728213604231.png)

## 1.首先

运行myfileBlogToHexo_Post.exe  将写的blog文档添加hexo可使用的抬头  初始化目录和标签

如果**目录和文件 加   #开头**则被排除在外

![image-20210728213822225](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210728213822225.png)

## 2.其次

进入hexo项目![image-20210728214002491](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210728214002491.png)

先清除  后发布到github

```vue
npm run deploy
```

如果是本地调试

```
npm start
```



# BAT 批处理脚本

```
cd /d d:\hexoBlog
npm run deploy
pause
start explorer d:\hexoBlog
```

参考链接 https://blog.csdn.net/whorus1/article/details/86646552


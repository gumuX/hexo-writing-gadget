

## 1.安装完成后不要打开进入注册机

点击patch，找到安装目录

![image-20210630123925497](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630123925497.png)



然后提示

![image-20210630124117800](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124117800.png)



然后回到注册机，确保以下几个选项是对应的

License为Ent[erp](http://www.downcc.com/k/erpapp/)rise

Products为Premium

Languages为Simplified Chinese（简体中文，其它语言版本请自选）

Resale Version为Site license

Your Name和Your Organization可以任意填写或者默认

![image-20210630124300563](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124300563.png)

上面几项设置好后，点击“Generate”，会自动生成一个注册码，如下图

![image-20210630124336208](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124336208.png)

打开软件

输入注册码

![image-20210630124425040](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124425040.png)

点激活  会提示因为激活服务器暂时不可使用.....我们选择“手动激活”将密钥输入到里面去

![image-20210630124544312](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124544312.png)

点击Generate

![image-20210630124630697](https://gitee.com/gumuX/figurebed/raw/master/images/image-20210630124630697.png)





参考网址http://www.downcc.com/soft/430673.html
